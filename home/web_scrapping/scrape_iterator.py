import json
import sys
import time
import traceback
from itertools import count
from re import match

import redis
import tqdm
from asgiref.sync import async_to_sync
from celery_progress.websockets.backend import channel_layer
from selenium.common import NoSuchElementException, StaleElementReferenceException, ElementNotInteractableException, \
    ElementClickInterceptedException, TimeoutException, WebDriverException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from home.web_scrapping.date_time import current_time
from home.web_scrapping.ice2w_dict import moto_atv_brand_model_dict, reduced_moto_atv_brand_model_dict


class ScrapeIterator:

    def __init__(self, selenium_web_driver, **kwargs):
        self.selenium_web_driver = selenium_web_driver
        self.scrapped_data_dict = {'data': []}
        self.total_items: int = 1
        self.total_pages: int = 1
        self.selected_page = 0
        self.wait_after_loading = 2
        self.wait_for_setup = 5
        self.kwargs = kwargs
        self.redis_client = redis.Redis(host='localhost', port=6379, db=0)

    def __iter__(self):
        return self

    def __next__(self):
        self.selected_page += 1
        try:
            max_wait_time = 10
            wait = WebDriverWait(self.selenium_web_driver, max_wait_time)
            element = wait.until(expected_conditions
                                 .visibility_of_element_located((By.CSS_SELECTOR, 'span[data-testid="total-count"]')))
            add_total_2 = element.text.split(' ')[2]
            add_total_3 = element.text.split(' ')[3].replace('.', '')

            if add_total_2.isdigit():
                self.total_items = int(add_total_2)
            elif add_total_3.isdigit():
                self.total_items = int(add_total_3)
            if self.total_items // 50 > 0:
                self.total_pages = self.total_items // 50

            pagination_elements = self.selenium_web_driver.find_elements(
                By.CSS_SELECTOR, 'a[data-testid^="pagination-link-"]')
            page_numbers = [int(el.text) for el in pagination_elements]
            highest_page_number = max(page_numbers)

            if highest_page_number > 1:
                self.total_pages = highest_page_number

            self.redis_client.set('progress_page', json.dumps({
                'value': round(self.selected_page / self.total_pages * 100),
                'description': f'{self.selected_page} ... {self.total_pages} pages',
                'source': 'progress_page'
            }))
            async_to_sync(channel_layer.group_send)(
                'progress', {
                    'task_id': self.kwargs['task_id'],
                    'type': 'progress.update',
                    'value': round(self.selected_page / self.total_pages * 100),
                    'description': f'{self.selected_page} ... {self.total_pages} pages',
                    'source': 'progress_page'
                })

        except (NoSuchElementException, TimeoutException, WebDriverException):
            tb = traceback.format_exc()
            print(f"Total count  TraceBack {tb}")
            raise StopIteration
        except (ValueError, ZeroDivisionError):
            tb = traceback.format_exc()
            print(f"Total count  TraceBack {tb}")

        if self.selected_page > self.total_pages:
            raise StopIteration
        if self.is_next_page_available():
            self.scrape_data()
            self.click_on_next_button()
        else:
            print("Finished for this type")
            raise StopIteration

    def __str__(self):
        return f'total pages: {self.total_pages}, total items: {self.total_items}'

    def is_next_page_available(self):
        try:
            self.selenium_web_driver.find_element(By.CSS_SELECTOR, 'a[data-testid="pagination-forward"]')
            return True
        except NoSuchElementException:
            tb = sys.exc_info()[0]
            print(f"Load Next Page TraceBack {tb}")
            return False

    def click_on_next_button(self):
        try:
            self.selenium_web_driver.find_element(By.CSS_SELECTOR, 'a[data-testid="pagination-forward"]').click()
            time.sleep(self.wait_after_loading)
        except (TimeoutException, StaleElementReferenceException, ElementClickInterceptedException):
            tb = sys.exc_info()[0]
            print(f"Scrape Data TraceBack {tb}")

    def scrape_data(self):
        source = 'progress_page_elements'
        image_url_regex = r"^(https.+)q=50$"
        ice_cards_loaded = 0
        try:
            page_body = self.selenium_web_driver.find_element(By.CSS_SELECTOR, 'body')
            page_body.send_keys(Keys.PAGE_DOWN)
            page_body.send_keys(Keys.PAGE_DOWN)
            page_body.send_keys(Keys.PAGE_DOWN)
            page_body.send_keys(Keys.PAGE_DOWN)
            page_body.send_keys(Keys.PAGE_DOWN)
            items_list = self.selenium_web_driver.find_elements(By.CSS_SELECTOR, 'div[data-cy="l-card"]')

            if self.total_items != '0':
                current_item = 0
                for element in items_list:
                    current_item += 1
                    self.redis_client.set(source, json.dumps({
                        'value': round(current_item / len(items_list) * 100),
                        'description': f'{current_item} ... {len(items_list)} adds',
                        'source': source
                    }))
                    async_to_sync(channel_layer.group_send)(
                        'progress', {
                            'task_id': self.kwargs['task_id'],
                            'type': 'progress.update',
                            'value': round(current_item / len(items_list) * 100),
                            'description': f'{current_item} ... {len(items_list)} adds',
                            'source': source
                        })

                    ice_cards_loaded += 1
                    if ice_cards_loaded == 5:
                        page_body.send_keys(Keys.PAGE_DOWN)
                        page_body.send_keys(Keys.PAGE_DOWN)
                        ice_cards_loaded = 0
                        time.sleep(0.3)
                    try:
                        ad_link = element.find_element(By.XPATH, "./a").get_attribute("href")
                        if 'extended_search_extended_category' in ad_link:
                            continue
                        ad_title = element.find_element(By.TAG_NAME, "h6").get_attribute("innerText")
                        ad_price_1 = element.find_element(By.CSS_SELECTOR, "p[data-testid='ad-price']").get_attribute(
                            "innerText").strip()
                        if ad_price_1 == "Schimb":
                            continue
                        if '€' in ad_price_1.split(' ')[1]:
                            ad_price = float(ad_price_1.split(' ')[0].replace(',', '.'))
                        else:
                            ad_price = float(ad_price_1.split(' ')[0] + ad_price_1.split(' ')[1].replace(',', '.'))

                        year_km = element.find_element(By.CLASS_NAME, "css-efx9z5").get_attribute("innerText")
                        if "-" in year_km:
                            ad_year = year_km.split("-")[0].rstrip()
                            ad_km = year_km.split("-")[1].rstrip()
                        else:
                            ad_year = year_km
                            ad_km = "-"
                        ad_location_date = element.find_element(By.CSS_SELECTOR,
                                                                "p[data-testid='location-date'").text.split('-')
                        ad_location = ad_location_date[0]
                        ad_relisting = ad_location_date[len(ad_location_date) - 1]
                        if "Azi" in ad_relisting:
                            ad_listing_date = current_time.split(' ')[0]
                        elif "Reactualizat" in ad_relisting:
                            ad_listing_date = current_time.split(' ')[0] + " Reactualizat"
                        else:
                            ad_listing_date = ad_relisting
                        ad_image_url = element.find_element(
                            By.CSS_SELECTOR, 'div.css-gl6djm img') \
                            .get_attribute('src')
                        image_urls = element.find_element(
                            By.CSS_SELECTOR, 'div.css-gl6djm img') \
                            .get_attribute('srcset')
                        all_image_urls = list(image_urls.split(' '))
                        ad_image_urls = [i for i in all_image_urls if match(image_url_regex, i)]
                        all_image_urls.insert(0, ad_image_url)
                    except (NoSuchElementException, StaleElementReferenceException, ElementNotInteractableException,
                            ElementClickInterceptedException):
                        tb = sys.exc_info()[0]
                        print(f"Scrape Data TraceBack {tb}")
                        continue

                    add_type = self.kwargs['ice_type']
                    add_brand = self.kwargs['brand']
                    add_model = None

                    if add_brand and add_type:
                        for model in reduced_moto_atv_brand_model_dict.get(add_brand).get(add_type):
                            if model in ad_link:
                                add_model = reduced_moto_atv_brand_model_dict.get(add_brand).get(add_type).get(model)

                        if add_model:
                            details_data = {
                                'title': ad_title,
                                'brand': add_brand,
                                'model': add_model,
                                'ice_type': add_type,
                                'price': ad_price,
                                'url': ad_link,
                                'year': ad_year,
                                'km': ad_km,
                                'location': ad_location,
                                'added_date': ad_listing_date,
                                'image_urls': ad_image_urls
                            }
                            self.scrapped_data_dict['data'].append(details_data)
        except (NoSuchElementException, StaleElementReferenceException, ElementNotInteractableException,
                ElementClickInterceptedException, TypeError):
            tb = sys.exc_info()[0]
            print(f"add_type = {add_type}")
            print(f"add_brand = {add_brand}")
            print(f"Scrape Data TraceBack {tb}")
            if len(self.scrapped_data_dict['data']) == 0 and self.total_items > 0:
                time.sleep(5)
                self.scrape_data()

    def get_data(self):
        return self.scrapped_data_dict
