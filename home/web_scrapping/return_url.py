from home.web_scrapping.IceEnum import MotoAtvEnum


def return_url(**kwargs):
    if kwargs['ice_type'] == MotoAtvEnum.ATV.value:
        kwargs['brand'] = "atv"
        url = f"https://www.olx.ro/d" \
              f"/auto-masini-moto-ambarcatiuni" \
              f"/{kwargs['ice_type']}/{kwargs['brand']}" \
              f"/?currency=EUR" \
              f"&search%5Bfilter_float_price:from%5D={kwargs['start_price']}" \
              f"&search%5Bfilter_float_year:from%5D={kwargs['year']}"

    else:
        url = f"https://www.olx.ro/d" \
              f"/auto-masini-moto-ambarcatiuni" \
              f"/{kwargs['ice_type']}/{kwargs['brand']}" \
              f"/?currency=EUR" \
              f"&search%5Bfilter_float_price:from%5D={kwargs['start_price']}" \
              f"&search%5Bfilter_float_year:from%5D={kwargs['year']}" \
              f"&search%5Bfilter_float_enginesize:from%5D=250"

    return url

# "https://www.olx.ro/auto-masini-moto-ambarcatiuni/motociclete-scutere-atv/" \
# "motociclete/q-KAWASAKI" \
# "/?currency=EUR&search%5B" \
# "filter_float_price:from%5D=500&search%5B" \
# "filter_float_year:from%5D=1990&search%5B" \
# "filter_float_enginesize:from%5D=250"
