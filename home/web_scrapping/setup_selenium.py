import sys
import time

from selenium import webdriver
from selenium.common import NoSuchElementException, TimeoutException, StaleElementReferenceException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait


def setup_selenium_webdriver(input_url):
    print(f"Start Selenium ")
    print(f"URL: {input_url}")
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--remote-debugging-port=9222")

    driver = webdriver.Chrome(options=chrome_options)

    driver.get(input_url)

    time.sleep(5)

    accept_tnc(driver)

    time.sleep(2)

    return driver


def accept_tnc(selenium_web_driver):
    print(f"Accept TNC ")
    try:
        WebDriverWait(selenium_web_driver, 10).until(
            EC.presence_of_element_located((By.XPATH, '//*[@id="onetrust-accept-btn-handler"]'))
        )
        selenium_web_driver.find_element(By.XPATH, '//*[@id="onetrust-accept-btn-handler"]').click()

    except (NoSuchElementException, TimeoutException, StaleElementReferenceException):
        tb = sys.exc_info()[0]
        print(f"Scrape Data TraceBack {tb}")


