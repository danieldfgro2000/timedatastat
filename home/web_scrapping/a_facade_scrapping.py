from django.core.cache import cache

from home.models import IceItem
from home.web_scrapping.db_tasks import save_scrapped_data_to_db, update_avg_price_for_all_ice_items, \
    delete_duplicates_of_the_day, \
    delete_old_data, delete_sold_items, create_statistic_data
from home.web_scrapping.return_url import return_url
from home.web_scrapping.scrape_iterator import ScrapeIterator
from home.web_scrapping.setup_selenium import setup_selenium_webdriver


def execute_model_scrapping(**kwargs):

    selenium_web_driver = setup_selenium_webdriver(return_url(**kwargs))

    scrape_iterator = ScrapeIterator(selenium_web_driver, **kwargs)

    for _ in scrape_iterator:
        continue

    create_statistic_data(ice_items=IceItem.objects.all())

    save_scrapped_data_to_db(scrape_iterator.get_data())

    update_avg_price_for_all_ice_items()

    delete_duplicates_of_the_day()

    delete_sold_items()

    delete_old_data()

    cache.clear()

    selenium_web_driver.quit()


if __name__ == '__main__':
    execute_model_scrapping(
        ice_type='motociclete',
        brand='honda',
        start_price=str(2000),
        year=str(2000),
        engine_size=str(250)
    )
