from enum import Enum


class MotoEnum(Enum):
    KAWASAKI = "kawasaki"
    HONDA = "honda"
    YAMAHA = "yamaha"
    SUZUKI = "suzuki"


class MotoAtvEnum(Enum):
    MOTOCICLETE = "motociclete"
    ATV = "scutere-atv-utv"
