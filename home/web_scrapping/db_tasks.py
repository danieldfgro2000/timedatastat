import os
import sys
from datetime import datetime, timedelta

import django
import tqdm
from django.core.exceptions import MultipleObjectsReturned
from django.db.models import Avg, Count, Min
from django.utils import timezone

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_data_stat.settings")
django.setup()

from home.models import IceItem, Image, Statistic


def save_scrapped_data_to_db(dict_ice_item):
    for item in dict_ice_item['data']:
        try:
            ice_item, created = IceItem.objects.update_or_create(
                title=item['title'],
                brand=item['brand'],
                model=item['model'],
                ice_type=item['ice_type'],
                price=item['price'],
                url=item['url'],
                year=item['year'],
                km=item['km'],
                location=item['location'],
                added_date=item['added_date']
            )

            for image_url in item['image_urls']:
                if image_url:
                    Image.objects.update_or_create(image_url=image_url, ice_item=ice_item)
        except MultipleObjectsReturned:
            tb = sys.exc_info()[0]
            print(f'Saving scrapped data exception: {tb}')


def update_avg_price_for_all_ice_items():
    brand_model_averages = (
        IceItem.objects.values("brand", "model", "year")
        .annotate(average_price=Avg("price"))
    )

    avg_dict = {(avg['brand'], avg['model'], avg['year']): avg for avg in brand_model_averages}

    ice_items = IceItem.objects.all()

    updated_objects = []

    for ice_item in ice_items:
        key = (ice_item.brand, ice_item.model, ice_item.year)
        if key in avg_dict:
            ice_item.avg_price = avg_dict[key]['average_price']
            updated_objects.append(ice_item)
    IceItem.objects.bulk_update(updated_objects, ['avg_price'], batch_size=500)


def create_statistic_data(ice_items):
    statistic, created = Statistic.objects.get_or_create(title=datetime.now().date())
    statistic.ice_items.add(*ice_items)


def delete_duplicates_of_the_day():
    duplicate_rows = (
        IceItem.objects.values('url', 'created_at')
        .annotate(min_id=Min('id'), d_count=Count('id'))
        .filter(d_count__gt=1)
    )

    if duplicate_rows.count() > 0:
        print(f"duplicate rows= {duplicate_rows.count()}")

    ids_to_delete = []

    for d in tqdm.tqdm(duplicate_rows):
        ids_to_delete.extend(IceItem.objects.filter(
            url=d['url'],
            created_at=d['created_at']
        ).exclude(id=d['min_id']).values_list('id', flat=True))

    IceItem.objects.filter(id__in=ids_to_delete).delete()


def delete_sold_items():
    IceItem.remove_sold_items()
    IceItem.update_sold_status()


def delete_old_data():
    now = timezone.now()
    sixty_days_ago = now - timedelta(days=60)
    old_items = IceItem.objects.filter(created_at__lt=sixty_days_ago)
    old_items.delete()


def delete_all_ice_items():
    print(f"start delete all ice items {datetime.now()}")
    IceItem.objects.all().delete()
    print(f"end delete all ice items {datetime.now()}")


def update_all_statistics():
    statistics = Statistic.objects.all()
    for stat in statistics:
        stat.update_statistic()


if __name__ == "__main__":
    # update_avg_price_for_all_ice_items()
    # delete_duplicates()
    # delete_old_data()
    update_all_statistics()
    # delete_all_ice_items()
