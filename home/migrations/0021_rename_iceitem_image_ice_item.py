# Generated by Django 4.2.2 on 2023-06-30 17:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0020_rename_iceid_image_iceitem_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='image',
            old_name='IceItem',
            new_name='ice_item',
        ),
    ]
