# Generated by Django 4.2.3 on 2023-09-10 11:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0027_alter_scrapparams_start_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='iceitem',
            name='km',
            field=models.TextField(default='-', max_length=30, null=True),
        ),
    ]
