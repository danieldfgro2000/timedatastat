import json
import sys
import time

import redis
from asgiref.sync import async_to_sync
from celery import shared_task
from celery.utils.log import get_task_logger
from celery_progress.backend import ProgressRecorder
from channels.layers import get_channel_layer
from selenium.common import NoSuchWindowException
from urllib3.exceptions import ProtocolError

from home.models import ScrapParams
from home.web_scrapping.IceEnum import MotoEnum, MotoAtvEnum
from home.web_scrapping.a_facade_scrapping import execute_model_scrapping

logger = get_task_logger(__name__)


@shared_task(bind=True)
def execute_model_scrapping_task(self):
    count = 1
    source = 'progress_ice_item'
    task_id = self.request.id
    total = len(MotoAtvEnum) * len(MotoEnum)
    scrapping_params = ScrapParams.objects.first()

    if scrapping_params:
        for moto_or_atv in MotoAtvEnum:
            for brand in MotoEnum:

                description = f'{count} ... {total} {moto_or_atv.name} {brand.name}'
                value = round(count / total * 100)

                send_ws_message(task_id, source, description, value)
                send_ws_message(task_id, 'progress_page', '', '')
                send_ws_message(task_id, 'progress_page_elements', '', '')

                try:
                    print('scrapping')
                    execute_model_scrapping(
                        task_id=task_id,
                        ice_type=moto_or_atv.value,
                        brand=brand.value,
                        start_price=str(scrapping_params.start_price),
                        year=str(scrapping_params.year),
                        engine_size=str(scrapping_params.engine_size)
                    )

                    count += 1

                    time.sleep(10)

                except (NoSuchWindowException, ProtocolError):
                    tb = sys.exc_info()[0]
                    print(f"Chrome exception {tb}")
    return 'Task completed'


def send_ws_message(task_id, source, description, value):
    channel_layer = get_channel_layer()
    redis_client = redis.Redis(host='localhost', port=6379, db=0)

    redis_client.set(source, json.dumps({
        'value': value,
        'description': description,
        'source': source
    }))

    async_to_sync(channel_layer.group_send)(
        'progress', {
            'task_id': task_id,
            'type': 'progress.update',
            'value': value,
            'description': description,
            'source': source
        })
