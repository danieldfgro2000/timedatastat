from django.urls import path

from home import views

urlpatterns = [
    path("", views.HomeTemplateView.as_view(), name='home-page'),
    path("create_scrapping_params/", views.ScrapParamsCreateView.as_view(), name='create-scrapping-params'),
    path("list_of_ice_items/", views.ScrappedListView.as_view(), name='list-of-ice-items'),
]
