import sys
from datetime import timedelta

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.paginator import Paginator, PageNotAnInteger, InvalidPage
from django.db.models import ExpressionWrapper, F, DecimalField, Case, When, DateField, IntegerField, DateTimeField, \
    fields
from django.db.models.functions import Cast, ExtractDay, Extract
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, ListView

from home.filters import ScrappedDataFilter
from home.forms import ScrappingParamsCreateForm
from home.models import IceItem, ScrapParams, Statistic, DaysAvailableExpression
from home.plot import plot_time_price_variance, plot_all_ice_items, plot_most_popular_model_by_year, \
    plot_most_popular_model
from home.tasks import execute_model_scrapping_task


class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        data = IceItem.objects.all().order_by('year')
        parameter_filter = ScrappedDataFilter(self.request.GET, queryset=data)
        filtered_data = parameter_filter.qs

        if filtered_data is not None:
            context['graph'] = plot_all_ice_items(filtered_data)
            context['graph_popular_model_by_year'] = plot_most_popular_model_by_year(filtered_data)
            context['graph_popular_model'] = plot_most_popular_model(filtered_data)

        statistic = Statistic.objects.first()
        if statistic is not None:
            statistic.update_time_price_variance(filtered_data)
            context['plot_time_price_var'] = plot_time_price_variance(statistic.time_price_variance)

        context['form_filters'] = parameter_filter.form
        return context


class ScrapParamsCreateView(LoginRequiredMixin, FormView):
    template_name = 'scrapping/create_scrapping_params.html'
    model = ScrapParams
    form_class = ScrappingParamsCreateForm
    success_url = reverse_lazy('home-page')

    def form_valid(self, form):
        form.instance.user = self.request.user
        ScrapParams.objects.all().delete()
        form.save()

        task = execute_model_scrapping_task.delay()
        self.request.session['task_id'] = task.id
        return super().form_valid(form)


def annotate_avg_range(queryset):
    return queryset.annotate(
        avg_low=ExpressionWrapper(F('avg_price') - 0.2 * F('avg_price'), output_field=DecimalField()),
        avg_high=ExpressionWrapper(F('avg_price') + 0.2 * F('avg_price'), output_field=DecimalField())
    )


class ScrappedListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'scrapping/list_of_ice_items.html'
    permission_required = 'home.view_list_of_ice_item'
    model = IceItem
    paginate_by = 9
    context_object_name = 'all_ice_items'

    def get(self, request, *args, **kwargs):
        params = request.GET.copy()
        if 'sort_by' not in request.GET:
            params['sort_by'] = 'price'
            return redirect(request.path_info + '?' + params.urlencode())
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        return IceItem.objects.all()

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)

        ice_items = IceItem.objects.all()

        ice_items = (ice_items.exclude(created_at__isnull=True).exclude(updated_at__isnull=True))
        ice_items_w_days_available = ice_items.annotate(days_available=DaysAvailableExpression())

        ice_items_w_avg_low_high = ice_items_w_days_available.annotate(
            avg_low=ExpressionWrapper(F('avg_price') - 0.2 * F('avg_price'), output_field=DecimalField()),
            avg_high=ExpressionWrapper(F('avg_price') + 0.2 * F('avg_price'), output_field=DecimalField())
        )

        parameter_filter = ScrappedDataFilter(self.request.GET, queryset=ice_items_w_avg_low_high)

        filter_params_preserved = self.request.GET.copy()

        filtered_ice_items = parameter_filter.qs

        page_number = self.request.GET.get('page')

        if filtered_ice_items.ordered:
            ice_feed = filtered_ice_items
        else:
            ice_feed = filtered_ice_items.distinct('url')

        paginator = Paginator(ice_feed, self.paginate_by)
        try:
            page_obj = paginator.get_page(page_number)
        except PageNotAnInteger:
            page_obj = paginator.get_page(1)
        except InvalidPage:
            page_obj = paginator.get_page(page_number.num_pages)

        data['page_obj'] = page_obj
        data['form_filters'] = parameter_filter.form
        data['filter_params_preserved'] = filter_params_preserved

        return data
