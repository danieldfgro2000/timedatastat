import logging
import sys
from datetime import datetime, timedelta
from typing import Type

from django.db import models
from django.db.models import Count, fields, ExpressionWrapper, F, Func
from django.utils.timezone import now
from sqlalchemy import exists

logger = logging.getLogger(__name__)


class IceItem(models.Model):
    SOLD, AVAILABLE = 'sold', 'available'
    SOLD_OPTIONS = [(SOLD, 'SOLD'), (AVAILABLE, 'AVAILABLE')]

    title = models.TextField(max_length=100)
    brand = models.TextField(max_length=100)
    model = models.TextField(max_length=100)
    ice_type = models.TextField(max_length=100)
    price = models.DecimalField(db_index=True, max_digits=15, decimal_places=2)
    avg_price = models.DecimalField(max_digits=15, decimal_places=2, null=True)
    url = models.URLField(db_index=True, max_length=200)
    year = models.TextField(max_length=30)
    km = models.TextField(max_length=30, null=True, default="-")
    location = models.TextField(max_length=100)
    added_date = models.TextField(max_length=30)
    sold_date = models.TextField(max_length=30)
    sold_status = models.CharField(max_length=9, null=True, choices=SOLD_OPTIONS)
    created_at = models.DateField(db_index=True, auto_now_add=True, null=True)
    updated_at = models.DateField(db_index=True, auto_now=True, null=True)

    def __str__(self):
        return f'{self.brand} {self.model} {self.price}'

    @classmethod
    def remove_sold_items(cls):
        today = datetime.now().date()
        five_days_ago = today - timedelta(days=5)
        print(f"start removing sold items {datetime.now()}")
        url_counts = cls.objects.values('url').annotate(count=Count('id'))
        for url_count in url_counts:
            if url_count['count'] > 1:
                recent_times = cls.objects.filter(url=url_count['url'], updated_at__gte=five_days_ago)
                if not recent_times.exists():
                    cls.objects.filter(url=url_count['url']).delete()
            else:
                item = cls.objects.get(url=url_count['url'])
                if item.updated_at < five_days_ago:
                    item.delete()
        print(f"removed sold items {datetime.now()}")

    @classmethod
    def update_sold_status(cls):
        print("UPDATING sold status")
        yesterday = datetime.now().date() - timedelta(days=2)
        items = cls.objects.all()

        sold_items = []
        available_items = []

        for i in items:
            if i.updated_at <= yesterday:
                i.sold_date = i.updated_at
                i.sold_status = cls.SOLD
                sold_items.append(i)
            else:
                i.sold_status = cls.AVAILABLE
                available_items.append(i)

        for chunk in [sold_items[i:i + 500] for i in range(0, len(sold_items), 500)]:
            cls.objects.bulk_update(chunk, ['sold_status', 'sold_date'])

        for chunk in [available_items[i:i + 500] for i in range(0, len(available_items), 500)]:
            cls.objects.bulk_update(chunk, ['sold_status'])


class Image(models.Model):
    image_url = models.TextField(max_length=100)
    ice_item = models.ForeignKey(IceItem, on_delete=models.CASCADE, null=True)


class ScrapParams(models.Model):
    start_price = models.DecimalField(max_digits=10, decimal_places=0, default=2000)
    year = models.IntegerField(default=2000)
    engine_size = models.IntegerField(default=250)


class Statistic(models.Model):
    title = models.DateField(auto_now_add=True)
    ice_items = models.ManyToManyField(IceItem)

    time_price_variance = models.JSONField(blank=True, null=True)

    def update_time_price_variance(self, filtered_ice_items=None):
        if filtered_ice_items:
            group_ice_items = filtered_ice_items.values('brand', 'model', 'year', 'url', 'avg_price', 'updated_at')
        else:
            group_ice_items = self.ice_items.values('brand', 'model', 'year', 'url', 'avg_price', 'updated_at')
        try:
            time_price_variance = [{
                'brand': item['brand'],
                'model': item['model'],
                'year': item['year'],
                'time': item['updated_at'].strftime("%Y-%m-%d"),
                'avg_price': float(item['avg_price'])
            } for item in group_ice_items]
            self.time_price_variance = time_price_variance
        except TypeError:
            tb = sys.exc_info()[0]
            print(f'update_time_price_variance exception: {tb}')

    def update_statistic(self):
        self.update_time_price_variance()
        self.save()
        # delete all entries except the last one
        Statistic.objects.exclude(id=self.id).delete()


class DaysAvailableExpression(models.Expression):
    def __init__(self, **extra):
        super().__init__(output_field=fields.IntegerField(), **extra)

    def as_sql(self, compiler, connection):
        return """
            (EXTRACT(EPOCH FROM updated_at) - EXTRACT(EPOCH FROM created_at)) / 86400 + 1
        """, []