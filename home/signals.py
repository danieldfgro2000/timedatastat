from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import IceItem, Statistic


@receiver(post_save, sender=IceItem)
def update_statistics(sender, instance, **kwargs):
    related_statistics = Statistic.objects.filter(ice_items__in=[instance])
    for statistic in related_statistics:
        statistic.update_statistic()
