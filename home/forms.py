from django import forms
from django.forms import NumberInput

from home.models import ScrapParams


class ScrappingParamsCreateForm(forms.ModelForm):
    class Meta:
        model = ScrapParams

        fields = [
            'start_price',
            'year',
            'engine_size'
        ]

        widgets = {
            'start_price': NumberInput(attrs={'placeholder': 'Starting price', 'class': 'form-control'}),
            'year': NumberInput(attrs={'placeholder': 'From this year onwards', 'class': 'form-control'}),
            'engine_size': NumberInput(attrs={'placeholder': 'Engine displacement', 'class': 'form-control'}),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        # if ScrapParams.objects.all().count() > 1:
        #     ScrapParams.objects.delete()
        return cleaned_data
