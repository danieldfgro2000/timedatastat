from enum import Enum


class PriceFilterEnum(Enum):
    HIGH = "High"
    AVERAGE = "Average"
    LOW = "Low"
    NO_REFERENCE = "No reference"
