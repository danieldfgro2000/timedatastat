from datetime import date, timedelta

import django_filters
from django import forms
from django.db.models import Avg, F, DecimalField, ExpressionWrapper, Count, Max, Subquery, DateTimeField, OuterRef, Q
from django.forms import TextInput, NumberInput, DateField
from django.utils.timezone import now

from home.enums import PriceFilterEnum
from home.models import IceItem
from home.web_scrapping.IceEnum import MotoAtvEnum, MotoEnum
from home.web_scrapping.ice2w_dict import moto_atv_brand_model_dict


class ScrappedDataFilter(django_filters.FilterSet):
    ice_type = django_filters.ChoiceFilter(
        lookup_expr='contains',
        choices=[(t.value.rstrip('e'), t.name) for t in MotoAtvEnum],
        label='Type',
        empty_label=None,
        widget=forms.RadioSelect
    )

    brand = django_filters.ChoiceFilter(
        choices=[(t.value.lower(), t.name) for t in MotoEnum],
        empty_label=None,
        widget=forms.RadioSelect
    )

    model = django_filters.CharFilter(
        lookup_expr='icontains',
        label='Model',
        widget=TextInput(attrs={'class': 'form-control'})
    )

    year = django_filters.CharFilter(
        lookup_expr='contains',
        label='Year',
        widget=TextInput(attrs={'class': 'form-control'})
    )

    price = django_filters.NumberFilter(
        lookup_expr='gt',
        label='Price higher than',
        widget=NumberInput(attrs={'class': 'form-control'})
    )

    price_avg = django_filters.ChoiceFilter(
        choices=[(tag.name, tag.value) for tag in PriceFilterEnum],
        label="Price avg",
        method='filter_price_avg',
        empty_label=None,
        widget=forms.HiddenInput
    )

    sort_by = django_filters.ChoiceFilter(
        choices=[
            ('model', 'Model'),
            ('price', 'Price (Low to High'),
            ('-price', 'Price(High to Low'),
            ('sold', 'Sold'),
            ('available', 'Available')
        ],
        initial='price',
        method='filter_sort_by',
        label='Sort by',
        empty_label=None,
        widget=forms.RadioSelect
    )

    def filter_price_avg(self, queryset, name, value):

        value = PriceFilterEnum[value]
        queryset_w_avg = queryset.filter(avg_price__isnull=False)
        queryset_wo_avg = queryset.filter(avg_price__isnull=True)

        if value == PriceFilterEnum.NO_REFERENCE:
            if queryset_w_avg.count() > 1:
                return queryset_w_avg.filter(price=F('avg_price'))
            else:
                return queryset_wo_avg
        elif value == PriceFilterEnum.LOW:
            return queryset_w_avg.filter(price__lte=F('avg_price') - 0.2 * F('avg_price'))
        elif value == PriceFilterEnum.HIGH:
            return queryset_w_avg.filter(price__gte=F('avg_price') + 0.2 * F('avg_price'))
        else:
            return queryset_w_avg.filter(price__gte=F('avg_price') - 0.2 * F('avg_price'),
                                         price__lte=F('avg_price') + 0.2 * F('avg_price'))

    def filter_sort_by(self, queryset, name, value):

        subquery = queryset.values('url').annotate(max_date=Max('created_at')) \
            .filter(url=OuterRef('url')).values('max_date')
        filtered_queryset = queryset.filter(created_at=Subquery(subquery, output_field=DateField()))

        if value in ['model', 'price', '-price']:
            queryset = filtered_queryset.order_by(value)
        elif value in ['sold', 'available']:
            condition = Q(sold_status='sold') if value == 'sold' else Q(sold_status='available')
            queryset = filtered_queryset.filter(condition)

        return queryset

    class Meta:
        model = IceItem
        fields = ['ice_type', 'brand', 'model', 'year', 'price', 'sort_by']
