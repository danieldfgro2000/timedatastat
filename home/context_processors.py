from django.urls import reverse


def progress_context(request):
    task_id = request.session.get('task_id', None)
    return {'task_id': task_id}


def navbar_data(request):
    if request.path == reverse('home-page'):
        page_name = {
            'name': '',
            'url': reverse('home-page')
        }
    else:
        page_name = {
            'name': '',
            'url': reverse('list-of-ice-items')
        }
    return {'navbar_data': page_name}
