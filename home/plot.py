import json
from collections import defaultdict

import numpy as np
import plotly
import plotly.offline as opy
import plotly.graph_objects as go
from django.db.models import Count
from plotly.graph_objs import Scatter
from scipy.optimize import curve_fit

from home.models import IceItem


def plot_time_price_variance(time_price_variance):
    grouped_items = {}
    for item in time_price_variance:
        key = (item['brand'], item['model'], item['year'])
        if key not in grouped_items:
            grouped_items[key] = []
        grouped_items[key].append(item)

    fig = go.Figure()

    for (brand, model, year), items in grouped_items.items():
        times = [item['time'] for item in items]
        avg_prices = [item['avg_price'] for item in items]
        fig.add_trace(go.Scatter(x=times, y=avg_prices, mode='lines+markers', name=f"{brand} {model} {year}"))

    plot_div = opy.plot(fig, auto_open=False, output_type='div')

    return plot_div


def plot_all_ice_items(filtered_items):
    average_prices = [item.avg_price for item in filtered_items]
    years = [item.year for item in filtered_items]

    scatter = Scatter(x=years, y=average_prices, mode='lines+markers')

    return json.dumps(scatter, cls=plotly.utils.PlotlyJSONEncoder)


def get_most_popular_model_by_year(filtered_data):
    aggregated_data = (filtered_data
                       .values('year', 'model')
                       .annotate(model_count=Count('model'))
                       .order_by('year', '-model_count'))

    result = {}

    for item in aggregated_data:
        year = item['year']
        model = item['model']
        count = item['model_count']

        if year not in result:
            result[year] = {}
        result[year][model] = count

    return result


def gaussian(x, mean, amplitude, standard_deviation):
    return amplitude * np.exp(- (x - mean) ** 2 / (2 * standard_deviation ** 2))


def plot_most_popular_model_by_year(filtered_data):
    data = get_most_popular_model_by_year(filtered_data)
    traces = []

    model_data_map = defaultdict(list)
    for year, models_data in data.items():
        for model, count in models_data.items():
            model_data_map[model].append((year, count))

    annotations = []
    for model, yearly_data in model_data_map.items():
        years = [int(yd[0]) for yd in yearly_data if yd[0].isdigit()]
        counts = [yd[1] for yd in yearly_data]
        # Debug prints:
        # print(f"Model: {model}")
        # print(f"Years: {years}")
        # print(f"Counts: {counts}")

        if len(years) > 3:
            try:
                initial_guess = [np.mean(years), np.max(counts), 1.0]
                optimal_params, _ = curve_fit(gaussian, years, counts, p0=initial_guess, maxfev=10000)
                fitted_values = gaussian(np.array(years), *optimal_params)

                max_year = years[np.argmax(counts)]
                max_count = np.max(counts)

                annotations.append({
                    'font': dict(
                        family="Courier New, monospace",
                        size=16,
                        color="red"
                    ),
                    'x': max_year,
                    'y': max_count,
                    'xref': 'x',
                    'yref': 'y',
                    'text': model,
                    'showarrow': True,
                    'arrowhead': 4,
                    'ax': 0,
                    'ay': -40
                })
                trace = go.Scatter(x=years, y=fitted_values, mode='lines', name=model)
                traces.append(trace)
            except Exception as e:
                continue
                # print(f"Couldn't fit data for model {model} {e}")
        # else:
        #     trace = go.Scatter(x=years, y=counts, mode='markers', name=model)
        #     traces.append(trace)

    layout = go.Layout(
        title='Model Popularity by Year (Gaussian Representation)',
        xaxis=dict(title='Year'),
        yaxis=dict(title='Count'),
        annotations=annotations
    )

    fig = go.Figure(data=traces, layout=layout)
    return json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)


def get_most_popular_model(filtered_data):
    aggregated_data = (filtered_data
                       .values('model')
                       .annotate(model_count=Count('model'))
                       .order_by('-model_count'))
    result = {}

    for item in aggregated_data:
        model = item['model']
        count = item['model_count']
        result[model] = count

    return result


def plot_most_popular_model(filtered_data):
    data = get_most_popular_model(filtered_data)
    traces = []

    for model, count in data.items():
        trace = go.Scatter(x=[model], y=[count], mode='lines+markers', name=model)
        traces.append(trace)

    layout = go.Layout(
        title='Model Popularity Overal',
        xaxis=dict(title='Model'),
        yaxis=dict(title='Count')
    )

    fig = go.Figure(data=traces, layout=layout)
    return json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
