from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_data_stat.settings')

app = Celery('django_data_stat')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'run-scrape-data-daily': {
        'task': 'home.tasks.execute_model_scrapping_task',
        'schedule': crontab(hour='18', minute='10'),
        'options': {'expires': 60 * 60},  # in one hour
    }
}
