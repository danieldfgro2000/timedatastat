daphne django_data_stat.asgi:application&

celery -A django_data_stat worker -l info -n worker1@%h&
celery -A django_data_stat worker -l info -n worker2@%h&
celery -A django_data_stat worker -l info -n worker3@%h&

celery -A django_data_stat beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler