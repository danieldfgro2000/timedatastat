#!/bin/bash

cd django_projects || true
cd timedatastat || true

source venv/bin/activate || true

nohup daphne -u /tmp/daphne.sock django_data_stat.asgi:application &
nohup celery -A django_data_stat worker --loglevel=INFO &
nohup celery -A django_data_stat beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler &