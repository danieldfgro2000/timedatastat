document.addEventListener("DOMContentLoaded", function () {
    var elem = document.querySelector('.grid');

    if (elem) {
        var msnry = new Masonry(elem, {
            itemSelector: '.grid-item',
            percentPosition: true
        });
    }
});