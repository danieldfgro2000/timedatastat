from django.urls import path

from user_extend import views

urlpatterns = [
    path('create_user/', views.UserCreateView.as_view(), name='create-user'),
]
