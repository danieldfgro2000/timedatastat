from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm, PasswordResetForm
from django.contrib.auth.models import User
from django.forms import TextInput, EmailInput


class UserNewForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password1', 'password2']

        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter last name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter a valid email', 'class': 'form-control'})
        }


class AuthenticationNewForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Please enter a name'})
        self.fields['password'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Please enter a password'})


class PasswordChangeNewForm(PasswordChangeForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['old_password'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter old password'})
        self.fields['new_password1'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter a new password'})
        self.fields['new_password2'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please confirm the new password'})


class PasswordResetNewForm(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attr.update({'class': 'form-control', 'Placeholder': 'Please enter a valid email'})
