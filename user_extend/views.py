from datetime import datetime
from random import randint

from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views.generic import CreateView

from django_data_stat.settings import EMAIL_HOST_USER
from user_extend.forms import UserNewForm
from user_extend.models import UserHistory


class UserCreateView(CreateView):
    template_name = "user_extend/create_user.html"
    model = User
    form_class = UserNewForm
    success_url = reverse_lazy('home-page')

    def form_valid(self, form):
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.last_name = new_user.last_name.upper()
            new_user.username = f"{new_user.first_name}_{new_user.last_name.replace(' ', '').lower()}_{str(randint(1000, 9999))}"
            new_user.save()

            new_message = f'La data de {datetime.now()} a fost adaugat userul cu urmatoarele informatii' \
                          f'username: {new_user.username}, ' \
                          f'first_name: {new_user.first_name},' \
                          f'last_name: {new_user.last_name}' \
                          f'email: {new_user.email}'

            details_user = {
                'full_name': f'{new_user.first_name} {new_user.last_name}',
                'username': f'{new_user.username}'
            }
            subject = 'Contul tau a fost creat in aplicatie'
            message = get_template('user_extend/mail.html').render(details_user)
            mail = EmailMessage(subject, message, EMAIL_HOST_USER, [new_user.email])
            mail.content_subtype = 'html'
            mail.send()

            new_message = f'La data de {datetime.now()} a fost adaugat userul cu urmatoarele informatii' \
                          f' username: {new_user.username}, first_name: {new_user.first_name}, last_name: {new_user.last_name}' \
                          f'email: {new_user.email}'
            UserHistory.objects.create(message=new_message, created_at=datetime.now(), updated_at=datetime.now())
            return redirect('login')
