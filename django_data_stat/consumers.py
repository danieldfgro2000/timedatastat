import json
import logging

import redis
from channels.generic.websocket import AsyncWebsocketConsumer
from django.core.cache import cache

logger = logging.getLogger(__name__)

redis_client = redis.Redis(host='locahost', port=6379, db=0)


class ProgressConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        await self.channel_layer.group_add('progress', self.channel_name)

        await self.accept()

        await self.get_cache('progress_ice_item')
        await self.get_cache('progress_page')
        await self.get_cache('progress_page_elements')

    async def get_cache(self, source):
        data = cache.get(source)
        if data:
            await self.send(text_data=json.dumps({
                'progress': data.get('value'),
                'description': data.get('description'),
                'source': data.get('source')
            }))

    async def progress_update(self, event):
        cache.set(event['source'], event, timeout=60 * 60)
        await self.send(text_data=json.dumps({
            'progress': event['value'],
            'description': event['description'],
            'source': event['source']
        }))

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard("progress", self.channel_name)
