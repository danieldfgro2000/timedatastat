from . import consumers
from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
from django.urls import path, re_path

websocket_urlpatterns = [
    re_path(r'ws/progress/$', consumers.ProgressConsumer.as_asgi()),
]
