from __future__ import absolute_import, unicode_literals

from loc_celery import app as celery_app

__all__ = ('celery_app',)
